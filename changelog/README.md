# Changelog

## Mise en place dans un projet

1. `yarn add --dev auto-changelog`
2. On ajoute le script dans `package.json` :
```json
{
    "scripts": {
        "postversion": "auto-changelog -o CHANGELOG.md -t https://gitlab.etd-solutions.com/etd-solutions/coding-standards/raw/master/changelog/keepchangelog.hbs -- breaking-pattern 'BREAKING CHANGE:' --commit-limit 10 --tag-prefix 'v' --sort-commits revelance --release-summary --ignore-commit-pattern '^(debug|test|boum|bim|changelog)$' && git add CHANGELOG.md && git commit -o ./CHANGELOG.md -m 'changelog' && git push --follow-tags"
    }
}
```

## C'est quoi un changelog?

Voir [keepachangelog.com](https://keepachangelog.com).

## Comment ça marche ?

On utilise [auto-changelog](https://github.com/CookPete/auto-changelog) pour générer automatiquement les changelogs.

Il analyse l'historique des commits git et génère un changelog en se basant sur les versions taggées (`git tag`), les pull requests mergées et les issues fermées.

On utilise `keepchangelog.hbs` comme template commun à tous les projets.

## Pourquoi on en a besoin ?

Pour que chaun puisse voir plus facilement ce qui a changé entre les versions (ou releases) d'un projet.

## Qui en a besoin ?

Toute le monde. Que se soit nos clients comme nos collègues. 